from django.contrib import admin
from .models import UserProfile, Posts, Comments


class PostsAdmin(admin.ModelAdmin):
    models = Posts
    list_display = ('id', 'user', 'post_text', 'pub_date')
    list_filter = ('user', 'pub_date',)


class CommentsAdmin(admin.ModelAdmin):
    models = Comments
    list_display = ('id', 'user', 'post', 'comment_text', 'date')


class UserProfileAdmin(admin.ModelAdmin):
    models = UserProfile
    list_display = ('id', 'full_name', 'date_of_birth', 'about_me', 'photo')

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Posts, PostsAdmin)
admin.site.register(Comments, CommentsAdmin)