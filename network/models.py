from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User)

    full_name = models.CharField(max_length=200)
    date_of_birth = models.DateField('date')
    about_me = models.CharField(max_length=3000)
    photo = models.ImageField(upload_to='user_photo', blank=True)

    def __str__(self):
        return self.user.email


class Posts(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    post_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published post')

    def __str__(self):
        return self.post_text


class Comments(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    post = models.ForeignKey(Posts, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=200)
    date = models.DateTimeField('date published')

    def __str__(self):
        return self.comment_text
